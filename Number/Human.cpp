#include "Human.h"
#include <iomanip>
#include <iostream>

using namespace std;

Human::Human() 
{
	name = -1;
	nameHuman = "None";
}

Human::Human(int _name, string _nameHuman) { 
	this->name = _name;
	this->nameHuman = _nameHuman;
}

void Human::printInfo() 
{
	cout << " | Name: " << name << setw(8) << " | Human: " << nameHuman << endl;
}

int Human::getName() 
{
	return name;
}

string Human::nameOfHuman() 
{
	return nameHuman;
}