#include "Header.h"

using namespace std;

Number CreateNewNumber(int lastName)
{
	string id;
	cout << "Enter id of new room: ";
	cin >> id;
	Number newRoom(id, lastName++, "None");
	cout << "Done." << endl << endl;
	return newRoom;
}