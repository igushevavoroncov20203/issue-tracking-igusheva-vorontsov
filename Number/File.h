#pragma once
#include <string>
#include "Human.h"
#include <vector>
#include "Number.h"

using namespace std;

class File
{
private:
	string nameHumanFile = "humans.txt"; // ���� ��� �������� �� � ������
	string nameNumberFile = "numbers.txt"; // �� � ��������
public:
	File(); // �����������
	vector<Number> getAllNumber(); // ��������� ���� �������
	vector<Human> getAllHumans(); // ���������� ���� �����
	void saveHumansData(vector<Human>); // ��������� �����
	void saveNumberData(vector<Number>); // ��������� �������
};