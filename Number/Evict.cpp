#include "Header.h"

using namespace std;

void Evict(vector<Number>& numbers, vector <Human>& humans)
{
	int name;
	cout << "Enter the number of the room to evict the person from" << endl;
	cin >> name;
	numbers[name - 1].setFree();
	Human f;
	for (int i = 0; i < humans.size()-1; i++)
	{
		if (name == humans[i].getName())
		{ 
			f = humans[i];
			humans[i] = humans[humans.size()-1];
			humans[humans.size() - 1] = f;
		}
	}
	humans.pop_back();
}