#include "Header.h"

using namespace std;

void PrintInfoAboutNumbers(vector<Number> numbers)
{
	cout << "=========== [All room] ============" << endl;
	for (auto v : numbers)
	{
		v.printInfo();
	}
	cout << endl << endl << "============ [Free only] ============" << endl;
	for (auto v : numbers)
	{
		v.printInfoIfIsFree();
	}
	cout << endl;
}