#include "File.h"
#include "Header.h"


File::File() 
{
	ifstream file; 
	file.open(nameHumanFile, ios::app); 
	file.close(); 
	file.open(nameNumberFile, ios::app); 
	file.close();
}

void File::saveHumansData (vector<Human> humans) 
{
	ofstream file;
	file.open(nameHumanFile, ios::trunc);
	file.close();
	file.open(nameHumanFile, ios::out| ios::app); 
	for (auto human : humans) 
	{
		file << human.getName() << " " << human.nameOfHuman() << endl; 
	}
	file.close(); 
}

void File::saveNumberData(vector<Number> numbers) 
{
	ofstream file(nameNumberFile); 
	for (auto number : numbers) 
	{
		file << number.getId() << " " << number.getName() << " " << number.nameOfHuman() << endl; 
	}
	file.close(); 
}

vector<Human> File::getAllHumans() 
{
	vector<Human> result;
	ifstream file(nameHumanFile);
	while (!file.eof())
	{
		string nameHuman;
		int name;
		file >> name >> nameHuman;
		if (nameHuman != "")
		{
			Human that(name, nameHuman); 
			result.push_back(that); 
		}
	}
	return result;
}


vector<Number> File::getAllNumber() 
{
	vector<Number> result; 
	ifstream file(nameNumberFile); 
	while (!file.eof()) 
	{
		string id, human;
		int name;
		file >> id >> name >> human; 
		if (id != "")
		{
			Number that(id, name, human); 
			result.push_back(that); 
		}
	}
	return result; 
}