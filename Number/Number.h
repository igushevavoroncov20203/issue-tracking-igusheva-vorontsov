#pragma once

#include<string>

using namespace std;

class Number {
private:
	string id;
	int name;
	string nameHuman;
	bool isFree;
public:
	Number();
	Number(string _id, int _name, string _nameHuman);
	void printInfo();
	void printInfoIfIsFree();
	int getName();
	string nameOfHuman();
	void checkIn(string name);
	void setFree();
	string getId();
};
