#include "Header.h"

using namespace std;

void CheckIn(vector<Number> &numbers, vector <Human> &humans)
{
	int name;
	string nameHuman;
	cout << "Enter the room number for the person to check in" << endl;
	cin >> name;
	cout << "Enter surname of person" << endl;
	cin >> nameHuman;
	numbers[name - 1].checkIn(nameHuman);
	Human that(name, nameHuman);
	humans.push_back(that);
}