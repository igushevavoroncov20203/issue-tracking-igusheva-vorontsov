#pragma once

#include "Number.h"
#include "File.h"
#include "Human.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <conio.h>
#include <windows.h>

void PrintInfoAboutNumbers (vector<Number> numbers);
void CheckIn(vector<Number> &numbers, vector <Human> &humans);
void Evict(vector<Number>& numbers, vector <Human>& humans);
void Search(vector <Human>& humans);
Number CreateNewNumber(int lastName);