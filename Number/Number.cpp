#include"Number.h"
#include <iomanip>
#include <iostream>

using namespace std;

Number::Number() 
{
	id = "";
	name = -1;
	nameHuman = "None";
	isFree = true;
}

Number::Number(string _id, int _name, string _nameHuman){ 
		this -> id = _id;
		this -> name = _name;
		this -> nameHuman = _nameHuman;
		if (_nameHuman == "None")
			isFree = true;
		else
			isFree = false;
}

void Number::printInfo() 
{
	if (isFree)
	{
		cout << "ID: " << id << setw(8) << " | Name: " << name << setw(8) << " | Free " << endl;
	}
	else 
	{
		cout << "ID: " << id << setw(8) << " | Name: " << name << setw(8) << " | Human: " << nameHuman << endl;
	}
}

void Number::printInfoIfIsFree() 
{
	if (isFree)
	{
		cout << "ID: " << id << setw(8) << " | Name: " << name << setw(8) << " | Free " << endl;
	}
}

int Number::getName() 
{
	return name;
}

string Number::nameOfHuman() 
{
	return nameHuman;
}

void Number::checkIn(string name) 
{
	nameHuman = name;
	isFree = false;
}

void Number::setFree() 
{
	nameHuman = "None";
	isFree = true;
}

string Number::getId() 
{
	return id;
}