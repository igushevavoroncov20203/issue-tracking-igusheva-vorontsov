#pragma once

#include<string>

using namespace std;

class Human {
private:
	int name;
	string nameHuman;
public:
	Human();
	Human(int _name, string _nameHuman);
	void printInfo();
	int getName();
	string nameOfHuman();
};