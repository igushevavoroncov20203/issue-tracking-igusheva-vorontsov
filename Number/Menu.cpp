#include "Header.h"

using namespace std;

const int n = 7;

string Menu[n];
int x, y;
int paragraph;

void GoToXY(int xpos, int ypos)
{
    COORD scrn;
    HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
    scrn.X = xpos; scrn.Y = ypos;
    SetConsoleCursorPosition(hOuput, scrn);
}

void SetColor(int text, int background)
{
    HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
}

void MenuToScreen()
{
    system("CLS");
    for (int i = 0; i < 7; i++)
    {
        GoToXY(::x, ::y + i);
        cout << ::Menu[i];
    }
    SetColor(2, 15);
    GoToXY(::x, ::y + ::paragraph);
    cout << ::Menu[::paragraph];
    SetColor(0, 15);
}

void main()
{
    vector <Number> numbers; 
    vector <Human> humans; 
    File file;	
    cout << "Retrieving information from a database of vacancies..." << endl;
    numbers = file.getAllNumber(); 
    cout << "Count of Numbers: " << numbers.size() << endl << endl;
    cout << "Retrieving information from a database of humans..." << endl;
    humans = file.getAllHumans(); 
    paragraph = 0;
    int flag;
    Menu[0] = "List of rooms";
    Menu[1] = "Create new room";
    Menu[2] = "Check a person into a room";    
    Menu[3] = "Evict a person from a room";
    Menu[4] = "Search for a number by the person who lives there";
    Menu[5] = "Save data";
    Menu[6] = "Exit";
    x = y = 5;
    SetColor(0, 15);
    MenuToScreen();
    do
    {
        flag = _getch();
        if (flag == 224)
        {
            flag = _getch();
            switch (flag)
            {
            case 80:
            {
                if (paragraph < 6)
                {
                    GoToXY(x, y + paragraph);
                    cout << Menu[paragraph];
                    paragraph++;
                    SetColor(2, 15);
                    GoToXY(x, y + paragraph);
                    cout << Menu[paragraph];
                    SetColor(0, 15);
                }
                break;
            }
            case 72:
            {
                if (paragraph > 0)
                {
                    GoToXY(x, y + paragraph);
                    cout << Menu[paragraph];
                    paragraph--;
                    SetColor(2, 15);
                    GoToXY(x, y + paragraph);
                    cout << Menu[paragraph];
                    SetColor(0, 15);
                }
                break;
            }
            }
        }
        else
        {
            if (flag == 13)
            {
                switch (paragraph)
                {
                case 0: {system("CLS");  PrintInfoAboutNumbers(numbers); system("pause"); break; }
                case 1: {system("CLS");  numbers.push_back(CreateNewNumber(numbers.size() + 1)); system("pause"); break; }
                case 2: {system("CLS");  CheckIn(numbers, humans); system("pause"); break; }
                case 3: {system("CLS");  Evict(numbers, humans); system("pause"); break; }
                case 4: {system("CLS");  Search(humans); system("pause"); break; }
                case 5: {system("CLS");
                    file.saveHumansData(humans);
                    file.saveNumberData(numbers);
                    system("pause"); break; }
                case 6: {system("CLS"); flag = 27; break; }
                }
            }
            MenuToScreen();
        }
    } while (flag != 27);
}