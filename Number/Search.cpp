#include "Header.h"

using namespace std;

void Search(vector <Human>& humans)
{
	string nameHuman;
	cout << "Enter the last name of the person you want to find" << endl;
	cin >> nameHuman;
	cout << "Result of search:" << endl;
	int flag = 0;
	for (auto human : humans) 
	{
		if (nameHuman == human.nameOfHuman())
		{
			cout << human.getName() << " " << human.nameOfHuman() << endl;
			flag = 1;
		}

	}
	if (flag == 0)
		cout << "People with these surnames were not found!" << endl;
}